<?php
global $banner_file;
global $face_file;
global $title;

if (!isset($banner_file)) $banner_file = "banners.txt";
if (!isset($face_file)) $face_file = "faces.txt";
$b = get_random_image($banner_file);
$f = get_random_image($face_file);

echo <<<HEREDOC
<div id="banner">
 <img src="$b" alt="Random Banner for $title">
 <h1>$title</h1>
 <img id="face" src="$f" alt="Random Face for $title">
</div>
HEREDOC;

