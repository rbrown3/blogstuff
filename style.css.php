body {font-family: Sans-Serif;}
#main {background-color:#7E3817;width:70%;max-width:1024px;margin: 0 auto 0 auto;padding:0 0 10em 0;}
#banner {position:relative;background-color:#fff;width:100%;margin: 2em 0 0 0;padding:0px;border-radius:0px;}
h1 {position:absolute;top:0;width:83%;font-size:4em;padding:0.2em 0 0 17%;margin:0 0 0 0;background-color:#ddd;opacity:0.6;color:#044C63;}
#today {font-size:2.7em;width:100%;background-color:white;text-align:center;margin:-2px 0 0 0;border:none;}
.post {background-color:#fff;width:90%;margin:0.5em auto 0.5em auto;padding:0.5em 0.5em 0.5em 0.5em;border-radius:15px;font-size:2.3em;}
#notice {background-color:#fff;width:90%;margin:0.5em auto 0.5em auto;padding:0.5em 0.5em 0.5em 0.5em;border-radius:15px;font-size:1.8em;font-weight:bold}
img {display:block;clear:both;width:100%;margin:0 auto 0 auto;}
#face {position:absolute;top:0;left:0;width:15%;border-radius:50%;}
.post > span > img {float:left;margin:0em 0.5em 0.5em 0em;border-radius:15px;width:50%;}
/* .post > img:hover {width:100%;} */
time {display:block;font-style:italic;font-weight:bold;font-size:0.8em;color:#aaa;padding:0 0 0 0;margin:1em 0 0 0;}
nav {font-size:2.3em;text-align:center;padding:0.8em 0 0 0;margin:0 0 1em 0;}
nav > a {color:blue;background-color:white;padding:0.5em;font-weight:bolder;}
a:visited {color:blue;}
br {display:block;margin-top:0.5em;}
.prev {float:left;font-size:1.0em; margin:-0.3em 0 0.5em 0;}
.next {float:right;font-size:1.0em; margin:-0.3em 0 0.5em 0; }

.post > span > img {width:100%;}
