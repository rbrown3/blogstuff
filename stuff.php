<?php

$blogstuff_path = dirname(__FILE__);

function get_random_entry($lines) {
   $num_lines = sizeof($lines);
   $r = mt_rand(0,$num_lines-1);
   return $lines[$r];
}

function get_random_line($filename) {
   $lines = explode("\n",file_get_contents($filename));
   $num_lines = sizeof($lines);
   $r = mt_rand(0,$num_lines-2);
   return $lines[$r];
}

function ob_include($include_file) {
   ob_start();
   include($include_file);
   return ob_get_clean();
}

// set_if_not_set global
function set($var,$val) { 
   if (!isset($GLOBALS[$var])) $GLOBALS[$var] = $val;
}


function format_posts($posts) {
   $posts_array = explode("\n",$posts);
   $posts = '';
   $count = 0;
   foreach ($posts_array as $post) {
      if (!preg_match("/<div/",$post) || !preg_match("/(jpg|gif)/i",$post)  ) {
         $posts .= "$post\n";
         continue;
      }
      $count++;
      $post_id = "<a name=\"post_$count\"></a>";

      $prev_count = $count-1;
      $prev_post = "#post_$prev_count";
      if ($prev_count<1) $prev_post = "/";
      $prev = "<nav class=\"prev\"><a href=\"$prev_post\">Previous</a></nav>";

      $next_count = $count+1;
      $next_post = "post_$next_count";
      if ($count >= count($posts_array)-1) $next_post = "banner";
      $next = "<nav class=\"next\"><a href=\"#$next_post\">Next</a></nav>";

      if (preg_match("/^\w*$/",$post)) break;
      $image_fullpath = preg_replace("/jpg.*/","jpg",$post);
      $image_fullpath = preg_replace("/gif.*/","gif",$image_fullpath);
      $image_fullpath = preg_replace("/.*\"/","",$image_fullpath);
      $image = preg_replace("/.*\//","",$image_fullpath);

      $post_type = preg_replace("/>.*/",">",$post);
      $txt   = preg_replace("/^.*?>.*?>/","",$post);
      $date  = preg_replace("/-[^0-9].*/","",$image);
  

      list($y,$m,$d) = explode("-",$date);

      if ($d % 10 == 1 && $d!=11) {
         $end = "st";
      } else if ($d % 10 == 2 && $d!=12) {
         $end = "nd";
      } else {
         $end = "th";
      }
      if ($d == '00') {
         $d = "";
      } else {
         $d = " $d<sup>$end</sup>";
      }

      $monthNum  = $m;
      //   $dateObj   = DateTime::createFromFormat('!m', $monthNum);
      //   $monthName = $dateObj->format('F');

      $months = explode(" ","January February March April May June July August September October November December");
      $monthName = $months[intval($m-1)];
      $t = "<time datetime=\"$date\">$monthName$d, $y</time>";

      $alt = preg_replace("/....-..-..-/","",$image);
      $alt = preg_replace("/-/"," ",$alt);
      $alt = preg_replace("/.jpg/","",$alt);
      $alt = preg_replace("/.gif/","",$alt);

      $txt = preg_replace("/<\/div>/","<div style=\"clear:both;\"></div></div>",$txt);

      $enable_click = false;
      $click = $enable_click ? "Click Image to Zoom" : "";
      $onclick = $enable_click ? " onclick=\"zoom(this)\"" : ""; 

      $enable_prevnext = false;
      $prevnext = $enable_prevnext ? "$prev$next" : "";

      $posts .= "$post_type$post_id$prevnext<div style=\"clear:both;\"></div><span>$click<img src=\"$image_fullpath\" alt=\"$alt\"$onclick></span>$t$txt\n";
   }
   return $posts;
}

