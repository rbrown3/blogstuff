

function blink(id) 
{
 var e = document.getElementById(id);
 e.style.transition = "opacity " + "0.2" + "s";

 count = 0;
 function helper() {
    if (count==3)
       e.style.opacity = 0.0;
    else if (count==4) {
       e.style.opacity = 1.0;
    }
    count = (count+1) % 5;
 }

 setInterval(helper,Math.floor(1000));

}

var blinker = blink("readme");

