

function slider(id,images,durationSeconds,transitionSeconds) 
{
 function fade() {
  var e = document.getElementById(id);
  e.style.transition = "opacity " + transitionSeconds + "s";
  e.style.opacity = 0.05;
  setTimeout(function() { 
   e.src = images[Math.floor(Math.random() * images.length)];
   e.style.opacity = 1;
  } , Math.floor(transitionSeconds*1000));
 }
 setInterval(fade,Math.floor(durationSeconds*1000));
}


function slider_crossfade(id,images,face_id,face_images,durationSeconds,transitionSeconds)
{

 var face = document.getElementById(face_id);

 var img1 = document.getElementById(id);
 
 var img2 = document.createElement('img');
 img2.id = id + '_slider';
 img2.src = img1.src;
 img2.style.transition = "opacity " + "1" + "s";
 img2.style.opacity = 0;
 img2.style.position = 'absolute';
 img2.style.top = 0;
 img2.style.left = 0;
 var parent = img1.parentNode;
 parent.appendChild(img2);

 var busy = false;
 function update(e) {
  if (busy) return;
  busy = true;
  setTimeout(function() {              
   img2.src = images[Math.floor(Math.random() * images.length)];
   setTimeout(function() {
    img2.style.opacity = 1;
    setTimeout(function() {
     img1.src=img2.src;
     setTimeout(function() {
      img2.style.opacity = 0;
      setTimeout(function() {
       busy = false;
      } , 1000);
     } , 1000); 
    } , 1000);
   } , 1000);
  } , 1000);
 }

 var state = 0;
 function updatimg2() {
  if (state!=0) return;
  state = 1;

  function statemachine() {
   switch (state) {
    case (1): img2.src = images[Math.floor(Math.random() * images.length)]; break;
    case (2): img2.style.opacity = 1; img2.style.zIndex  = 0; break;
    case (3): img1.src=img2.src; face.src = face_images[Math.floor(Math.random() * face_images.length)];  break;
    case (4): img2.style.opacity = 0; break;
    case (5): break;     // img2.style.zIndex  = -1; break; // move to back so Next link works
    case (6): break; // keep image stable for a little longer
    default: state = 0; break;
   }
   if (state!=0) {
    state++;
    setTimeout(statemachine, 1000); 
   }
  }
   
  statemachine();
 }

 // setInterval(function(){update(e)},Math.floor(durationSeconds*1000));
 setInterval(updatimg2 , Math.floor(durationSeconds*1000));
}




<?php echo 'var banner_images = ["' . implode('","',glob('images/*banner.jpg')) . '"];' . "\n"; ?>
<?php echo 'var face_images = ["' . implode('","',glob('images/*face.jpg')) . '"];' . "\n"; ?>
var banner_slider = slider_crossfade("banner_img",banner_images,"face",face_images,3,0.5);

