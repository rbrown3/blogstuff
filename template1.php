<?php

// ==============================================================
set("image_path","/images");
set("posts_path","./posts");
set("posts_file","$posts_path/home.php");
set("banner_file","images/*banner.jpg");
set("face_file","images/*face.jpg");

$css   = ob_include("$blogstuff_path/style.css.php");
$today = date("l, F j, Y");
$posts = format_posts(ob_include($posts_file));
$js    = ob_include("$blogstuff_path/today.js.php");
$js    .= ob_include("$blogstuff_path/slider.js.php");
$js    .= ob_include("$blogstuff_path/zoom.js.php");
$js    .= ob_include("$blogstuff_path/blink.js.php");

$banner_image = get_random_entry(glob($banner_file));
$banner_face  = get_random_entry(glob($face_file));

if (isset($notice) && $notice!="") {
   $notice = '<div id="notice">' . $notice . '</div>';
} else {
   $notice = '';
}

// ==============================================================

echo <<<HEREDOC
<!doctype html>
<html lang="en">
<head>
<meta charset=utf-8>
<title>$title</title>
<style media="screen" type="text/css">
$css
</style>
</head>

<body><div id="main" style="padding-top:0.05em;">
<div id="today">Today is <b>$today</b></div>
$notice

<nav>
$nav
</nav>

<div id="banner">
 <a name="banner"></a>
 <img id="banner_img" src="$banner_image" alt="$title">
 <h1>$h1</h1>
 <img id="face" src="$banner_face" alt="Random Face for $h1">
 <!-- <div class="next" style="position:absolute;top:0px;right:0px;;font-size:3.5em;padding:0.3em 0.3em 0 0;"><a href="#post_1">Next</a></div> -->
</div>

$posts
</div> <!-- main -->

<script>
$js
</script>

</body>
</html>
HEREDOC;
