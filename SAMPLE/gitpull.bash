#!/bin/bash
# gitpull.bash
# Find all git repositories and pull them

repo_list=`find . |grep .git$ |sed 's#.git$##'`
for repo in $repo_list; do
    pushd $repo > /dev/null
    echo "REPO: $repo"
    git pull
    popd > /dev/null
done

